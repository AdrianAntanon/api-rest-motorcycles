const express = require("express"); // es el nostre servidor web
const cors = require("cors"); // ens habilita el cors recordes el bicing???
const bodyParser = require("body-parser"); // per a poder rebre jsons en el body de la resposta

const app = express();
const baseUrl = "/apimotorcycles";
app.use(cors());

app.use(bodyParser.urlencoded({ extended: false })); //per a poder rebre json en el reuest
app.use(bodyParser.json());
//La configuració de la meva bbdd
ddbbConfig = {
  user: "adrian-antanyon-7e3",
  host: "postgresql-adrian-antanyon-7e3.alwaysdata.net",
  database: "adrian-antanyon-7e3_motorcycles_db",
  password: "Contra123",
  port: 5432,
};
//El pool es un congunt de conexions
const Pool = require("pg").Pool;
const pool = new Pool(ddbbConfig);

//Exemple endPoint
//Quan accedint a http://localhost:3000/apimotorcycles/test   ens saludará
const getTest = (request, response) => {
  response.send("Hola test");
};
app.get(baseUrl + "/test", getTest);

const getMotos = (request, response) => {
  let brand = request.query.marca || "";

  const capitalizeFirstLetter = (motorcycleBrand) => {
    return motorcycleBrand.charAt(0).toUpperCase() + motorcycleBrand.slice(1);
  };

  let myQuery;
  console.log(brand);
  if (brand === "") {
    myQuery = "SELECT * FROM  motos;";
  } else {
    myQuery = `SELECT * FROM  motos WHERE marca='${capitalizeFirstLetter(
      brand
    )}';`;
  }

  pool.query(myQuery, (error, results) => {
    if (error) {
      throw error;
    }
    response.status(200).json(results.rows);
  });
};
app.get(baseUrl + "/getmotos", getMotos);

const deleteMoto = (request, response) => {
  const motorcycleId = request.params.id;

  var consulta = `DELETE FROM motos WHERE id=${motorcycleId};`;
  pool.query(consulta, (error, results) => {
    if (error) {
      throw error;
    }
    if (results.rowCount != 0) {
      response.status(200).json(results.rows);
    } else {
      response.status(200).json({ response: "No s'ha trobat aquesta moto" });
    }
  });
};

//aqui un exemple de post que pasarem per body
const addMoto = (request, response) => {
  console.log(request.body);
  //const { marca, modelo, year, foto, precio } = request.body
  //console.log(marca, modelo, year, foto, precio);
  response.send("Hola aixo es el que m'arriva" + JSON.stringify(request.body));
};

app.post(baseUrl + "/moto", addMoto);
app.delete(baseUrl + "/deleteMoto/:id", deleteMoto);

//Inicialitzem el servei
const PORT = process.env.PORT || 3000; // Port
const IP = process.env.IP || null; // IP

app.listen(PORT, IP, () => {
  console.log("El servidor está inicialitzat en el puerto " + PORT);
});
